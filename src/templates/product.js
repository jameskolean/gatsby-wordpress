import React from 'react'
import { graphql } from 'gatsby'

const Product = ({ data }) => {
  const product = data.wordpress.productBy
  return (
    <section>
      <h2>Product</h2>
      <h3>{data.wordpress.productBy.title}</h3>
      <p
        dangerouslySetInnerHTML={{
          __html: product.content,
        }}
      />
      <dl>
        <dt>Price</dt>
        <dd>{product.details.price}</dd>
        <dt>SKU</dt>
        <dd>{product.details.sku}</dd>
      </dl>
    </section>
  )
}

export default Product

export const query = graphql`
  query($slug: String!) {
    wordpress {
      productBy(slug: $slug) {
        id
        details {
          price
          sku
        }
        title
        content
      }
    }
  }
`
